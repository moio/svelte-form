# Svelte Form
![](https://gitlab.com/moio/svelte-form/-/raw/master/svelte-form.gif)
## Installation

### docker-compose

```bash
docker-compose up
```

### npm

```bash
npm install
npm run dev
```

### yarn

```bash
yarn
yarn dev
```

## Usage
1. first step
    * firstname: `optional`
    * username: jo
    * email: jo@example.com
2. second step
    * url: `optional` or example.com
3. third step
    * password: secret
