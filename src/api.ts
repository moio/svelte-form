import { stall } from './utils';

export const api = {
	validate: (form: any) => {
		return stall({ isValid: chekForm(form) });
	},
};

const chekForm = (form: any) => {
	if (form.username === 'jo' && form.email === 'jo@example.com') {
		return true;
	} else if (form.url === "" || form.url === 'example.com') {
		return true;
	} else if (form.password === 'secret') {
		return true;
	} else {
		return false;
	}
};
