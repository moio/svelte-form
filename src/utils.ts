
export const range = (num: number) => {
	return [...Array(num).keys()]
}

export const stall = async (value: any) => {
	const delay = (ms: number) => new Promise((res) => setTimeout(res, ms));
	await delay(2000);
	return value;
};
