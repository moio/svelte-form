// store.js
import { writable } from 'svelte/store';

export const user = writable({
	username: '',
	firstname: '',
	email: '',
	url: '',
	password: ''
});

const createCounter = () => {
	const { subscribe, update, set } = writable(0);

	return {
		subscribe,
		increment: () => update((x: number) => x + 1),
		set: (value: number) => set(value),
	};
};

const createBooleanArray = () => {
	const { subscribe, update } = writable([]);

	return {
		subscribe,
		setTrue: (index:number) => update((arr: boolean[]) => {
			const tmp = arr.filter((_: boolean, i: number) => i < index);
			tmp[index] = true;
			return tmp;
		}),
		setFalse: (index:number) => update((arr: boolean[]) => {
			const tmp = arr.filter((_: boolean, i: number) => i < index);
			tmp[index] = false;
			return tmp;
		}),
	};
};

export const stepStatus = createBooleanArray();
export const currentStep = createCounter();
