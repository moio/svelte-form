FROM node
WORKDIR /app
COPY . .
ENV HOST=0.0.0.0
ENTRYPOINT yarn && yarn dev
